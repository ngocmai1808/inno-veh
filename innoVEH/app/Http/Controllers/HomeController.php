<?php

namespace App\Http\Controllers;
use App\Motorbike;
use App\Review;
use App\User;
use App\Image;
use App\Promotion;
use Hash;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getindex()
    {
        $nhaxe = User::where('role_id',2)->paginate(2);
        $sale = Promotion::all();
        $nhaxemoi = User::where('id>10');
       return view('trangchu', compact('nhaxe','sale','nhaxemoi'));
    }
    public function getOwner()
    {
        return view('chitietnhaxe' );
    }
//    public function getSearch(Request $req){
//        $product = Motorbike::where('name','like', '%'.$req->key.'%')->count();
//        return view('danhsachnhaxe');
//    }
    public function getLogin(){
        return view('dangnhap');
    }

    public function postLogin(Request $req){
        $this->validate($req,
            [
                'username' => 'required',
                'password' => 'required|min:6|max:50',
            ],
            [
                'uesername.required' => 'Vui lòng nhập tài khoản',
                'password.required' => 'Vui lòng nhập pass',
            ]);
        
    }
    public function getRegister(){
        return view('dangky');
    }

    public function postRegister(Request $request){
        $this->validate($request,
            [
                'email' =>'required|email|unique:users,email',
                'password' => 'required|min:6|max:50',
                'phonenumber' => 'required',
                'repassword' =>'required|same:password',
                'username' => 'required|unique:users,name',
                'address' => 'required',
            ],
            [
                'email.required' => 'Vui lòng nhập email',
                'email.unique' => 'Email đã có được sử dụng',
                'email.email' => 'Bạn vui lòng nhập đúng định dạng',
                'password.required' =>'Bạn không thể để trống ô này',
                'password.min' => 'Mật khẩu yêu cầu đủ 6 ký tự',
                'username.unique' =>'Tên này đã được sử dụng',
                'username.required' =>'Bạn vui lòng điền đầy đủ thông tin',
                'repassword.same' => 'Xin vui lòng nhập lại',
                'address.required' => 'Xin vui lòng điền đầy đủ thông tin'
            ]
        );
        $user = new User();
        $user->username = $request -> username;
        $user->password = Hash::make($request -> password);
        $user->phone = $request -> phone;
        $user->address = $request -> address;
        $user->save();
//        return redirect('trangchu')->with('thanhcong','Tạo tài khoản thành công');
    }
}
