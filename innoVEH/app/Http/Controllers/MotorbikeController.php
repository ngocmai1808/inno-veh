<?php

namespace App\Http\Controllers;
use App\Motorbike;
use App\Http\Requests\MotorbikeRequest;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;


class MotorbikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function danhsachnhaxe()
    {
        $mortorbike = User::where('role_id', 2)->paginate(3);
        return view('danhsachnhaxe', compact('mortorbike'));
    }

    public function getChitiet(Request $req)
    {
        $sanpham = User::where('id', $req->id)->first();
        return view('chitietnhaxe', compact('sanpham') );
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function getAll($id){
        $motorbikes = Motorbike::where('user_id',$id)->get();
        return view('admin.list_motorbike',compact(['motorbikes', 'id']));
     }
     public function getAdd($id){
        return view('admin.add_motorbike',compact('id'));
     }
     public function getEdit($id){
        $motorbikes = Motorbike::find($id);
        return view('admin.edit_motorbike',['motorbike'=>$motorbikes,'id'=>$id]);
     }
     public function postEdit(MotorbikeRequest $request ,$id){
            $post = Motorbike::find($id);
            $image='upload/'.$request->image;

            $post->price_day = $request->price_day;
            $post->price_week= $request->price_week;
            $post->detail= $request->detail;
            $post->required=$request->required;
            $post->status= $request->status;
            $post->save();
            $user_id = $post->user_id;
            return redirect()->route('getList',[$user_id]);
     }
     
    public function postAdd(MotorbikeRequest $request){
       if($request->hasFile('image')){

        $image = $request->file('image');
        //upload anh
         $image->move("upload/owner/motorbike",$image->getClientOriginalName());

         $add = Motorbike::create([
        'price_day' => $request->price_day,
        'user_id'=>$request->user_id,
        'price_week' => $request->price_week,
        'required' => $request->required,
        'status' => $request->status,
        'detail' => $request->detail,
        'image'=>$image->getClientOriginalName(),
      ]);

         $id = $request->user_id;
       return redirect()->route('getList',[$id]);
      }    
    }
    public function delete($id){
        $motorbike = Motorbike::find($id); 
        $user_id = $motorbike['user_id'];
        $motorbike->delete($id);
    return redirect()->back();

    }
}
