@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-8 col-xs-offset-2">
	<div class="panel panel-primary">
		<div class="panel-heading">Add Owner</div>
		<div class="panel-body">
			<!--  -->
			 @if ($errors->any())
                    <ul class="error-form">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
			<form method="post" action="{{ route('postAdd')}}" enctype= "multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">name</div>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" placeholder="vui lòng nhập tên nhà xe">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">phone</div>
				<div class="col-md-9">
					<input type="text" name="phone" class="form-control" placeholder="vui lòng nhập số điện thoại ">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">email</div>
				<div class="col-md-9">
					<input type="email" name="email" class="form-control" placeholder="vui lòng nhập email">
				</div>
			</div>
			<!-- end row -->
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">role</div>
				<div class="col-md-9">
					<input type="text" name="role_id" class="form-control" placeholder="giá trị của chủ xe là 2,người dùng là 3">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">password</div>
				<div class="col-md-9">
					<input type="password" name="password" class="form-control" placeholder="vui lòng nhập  mật khẩu dăng nhập">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">status</div>
				<!-- <div class="col-md-9">
					<input type="text" name="status" class="form-control" placeholder="trạng thái nhập giá trị 0 hoặc 1 " >
				</div> -->
				<label class="col-md-3"><input type="radio" name="status" value="1">Visible</label>
				<label class="col-md-3"><input type="radio" name="status" value="0">Invisible</label>
			</div>
			<!-- end row -->
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">address</div>
				<div class="col-md-9">
					<input type="text" name="address" class="form-control" placeholder="vui lòng nhập dịa chỉ " >
				</div>
			</div>
			<!-- end row -->
		
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">detail</div>
				<div class="col-md-9">
					<textarea name="detail" class="form-control" style="height:250px;">	
	
					</textarea>
					<script type="text/javascript">
					CKEDITOR.replace('detail');
					</script>
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">image</div>
				<div class="col-md-9">
					<input type="file" name="image">
				</div>
			</div>
			
			<!-- end row -->			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3"></div>
				<div class="col-md-9">
					<input type="submit" class="btn btn-primary" value="Process">
				</div>
			</div>
			<!-- end row -->
			</form>
		</div>
	</div>
</div>
@endsection