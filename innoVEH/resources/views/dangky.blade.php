<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Au Register Forms by Colorlib</title>

    <!-- Icons font CSS-->
    <link href="{{URL::asset('vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    <link href="{{URL::asset('vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{{URL::asset('vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{URL::asset('vendor/datepicker/daterangepicker.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{URL::asset('css/register.css')}}" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-heading"></div>
                <div class="card-body">
                    <h2 class="title">Registration Form</h2>
                    {{--@if(count($errors)>0)--}}
                        {{--<div class="alert-danger">--}}
                            {{--@foreach($error->all() as $err)--}}
                                {{--{{$err}}--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--@endif--}}
                    {{--@if(Session::has('thanhcong'))--}}
                        {{--<div class="alert-success">{{Session::get('thanhcong')}}</div>--}}
                    {{--@endif()--}}
                    <form method="POST" action="{{URL::route('dangky')}}">
                        {{--<input type="hidden" name="_token" value ="{{csrf_token}}">--}}
                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder="Username" name="name">
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" type="password" placeholder="Password" name="password">
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" type="password" placeholder=" ConFirm Password" name="repassword">
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" type="number" placeholder=" Phone Number" name="phonenumber">
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder=" Address" name="address">
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" type="email" placeholder=" Email" name="email">
                        </div>

                        <div class="p-t-20">
                            <button class="btn btn--radius btn--green" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="{{URL::asset('vendor/jquery/jquery.min.js')}}"></script>
    <!-- Vendor JS-->
    <script src="{{URL::asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{URL::asset('vendor/datepicker/moment.min.js')}}"></script>
    <script src="{{URL::asset('vendor/datepicker/daterangepicker.js')}}"></script>

    <!-- Main JS-->
    <script src="{{URL::asset('js/global.js')}}"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
