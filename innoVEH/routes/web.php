<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index',function(){
    return view('trangchu');
});

Auth::routes();

Route::get('home',
    ["as" => "home", "uses" =>"HomeController@getindex"]);
Route::get('dangkychuxe', ['as' => 'dangkychuxe', 'uses' => 'UserController@create']);
Route::get('customer',['as'=>'customer', 'uses' => 'UserController@customer']);
Route::get('nhaxe', ['as'=> 'nhaxe', 'uses' => 'HomeController@getOwner']);
Route::get('danhgia', ['as' => 'danhgia', 'uses' => 'ReviewController@index']);
Route::get('chitiethoadon', ['as'=>'chitiethoadon', 'uses' => 'UserController@chitiethoadon']);
Route::get('doanhthu', ['as' => 'doanhthu', 'uses' => 'UserController@showdoanhthu']);
Route::get('danhsachnhaxe', ['as' => 'danhsachnhaxe', 'uses' => 'MotorbikeController@danhsachnhaxe']);
Route::get('danhsachthuexe', ['as' => 'danhsachthuexe', 'uses' => 'OrderController@danhsachthuexe']);
Route::get('datxe', ['as' => 'datxe', 'uses' => 'OrderController@datxe']);
Route::get('khuyenmai', ['as' => 'khuyenmai', 'uses' => 'OrderDetailController@khuyenmai']);
Route::get('lich', ['as' => 'lich', 'uses' => 'OrderController@lich']);
Route::get('bieudo', ['as' => 'bieudo', 'uses' => 'UserController@bieudo']);
Route::get('search',['as' =>'search', 'uses' => 'HomeController@getSearch']);
Route::get('xemchitiet',['as' => 'xemchitiet', 'uses' => 'MotorbikeController@getChitiet']);
Route::get('dangnhap' ,['as' => 'dangnhap', 'uses' => 'HomeController@getLogin']);
Route::get('dangky' ,['as' => 'dangky', 'uses' => 'HomeController@getRegister']);

Route::post('dangky' ,['as' => 'dangky', 'uses' => 'HomeController@postRegister']);
Route::post('dangnhap' ,['as' => 'dangnhap', 'uses' => 'HomeController@postLogin']);

Route::get('/admin','AuthController@getLogin')->name('login');
Route::post('/admin','AuthController@postLogin')->name('clogin');
Route::group(['prefix'=>'admin','middleware'=>['checkLogin']],function(){
		Route::get('/list','UserController@getLayout')->name('layout');
		//Route::get('/listRoleOwner/{id}','UserController@getLayoutOwner')->name('layoutOwner');
		Route::get('/logout','AuthController@logout')->name('logout');
		Route::get("/owner","UserController@getAll");
		// edit user 
		Route::get("/owner/edit/{id}","UserController@getEdit")->middleware('checkRole');
		// do edit 
		Route::post("/owner/edit/{id}","UserController@postEdit")->name('postEdit');
		// ad user 
		Route::get("/owner/add","UserController@getAdd");
		// do_addd
		Route::post("/owner/add","UserController@postAdd")->name('postAdd');
		// xoa 
		Route::get("/owner/delete/{id}","UserController@delete");
		//list motorbike
		Route::get("/motorbike/list/{id}","MotorbikeController@getAll")->name('getList');
		//get add motorbike 
		Route::get("/motorbike/add/{id}","MotorbikeController@getAdd");
		// do add
		Route::post("/motorbike/add","MotorbikeController@postAdd")->name('postAddMotorbike');
		//get edit motorbike 
		Route::get("/motorbike/edit/{id}","MotorbikeController@getEdit");
		// post edit motorbike
		Route::post("/motorbike/edit/{id}","MotorbikeController@postEdit")->name('postEditMotorbike');
		// delete motorbike 
		Route::get("motorbike/delete/{id}","MotorbikeController@delete");
		// user 
		Route::get("/user","UserController@getUserAll");
		// edit user 
		Route::get("/user/edit/{id}","UserController@getUserEdit");
		// do edit 
		Route::post("/user/edit/{id}","UserController@postUserEdit")->name('postUserEdit');
		
		// ad user "UserController@postUserEdit")->name('postUserEdit');
		Route::get("/user/add","UserController@getUserAdd");
		// do_addd
		Route::post("/user/add","UserController@postUserAdd")->name('postUserAdd');
		// xoa 
		Route::get("/user/delete/{id}","UserController@userdelete");
		
});	
