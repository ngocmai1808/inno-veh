<?php

namespace App\Http\Controllers;
use App\Promotion;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    public function getPromotionAll($id){
        $promotions = Promotion::where('user_id',$id)->get();
        return view('admin.list_promotion',compact(['promotions','id']));
    }
    public function getPromotionAdd ($id){
        return view('admin.add_promotion',compact('id'));
    }
    public function getPromotionEdit($id){
        $promotion = Promotion::find($id);
        return view('admin.edit_promotion',['promotion'=>$promotion,'id'=>$id]);
    }
    public function postPromotionEdit(Request $request ,$id){
        $post = Promotion::find($id);
        $post->name = $request->name;
        $post->value_p = $request->value_p;
        $post->save();
        $user_id = $post->user_id;
        return redirect()->route('getPromotionList',[$user_id]);
    }

    public function postPromotionAdd(Request $request){
        $add = Promotion::create([
            'user_id'=>$request->user_id,
            'name' => $request->name,
            'value_p' => $request->value_p,
        ]);
        $id = $request->user_id;
        return redirect()->route('getPromotionList',[$id]);
    }

    public function deletePromotion($id){
        $promotion = Promotion::find($id);
        $user_id = $promotion['user_id'];
        $promotion->delete($id);
        return redirect()->back();

    }
    // Owner
    public function getListAll($id){
        $promotions = Promotion::where('user_id',$id)->get();
        return view('owner.list_promotion',compact(['promotions','id']));
    }
    public function getPromotionAddOfOwner($id){
        return view('owner.add_promotion',compact('id'));
    }
    public function postPromotionAddOfOwner(Request $request){
        $add = Promotion::create([
            'user_id'=>$request->user_id,
            'name' => $request->name,
            'value_p' => $request->value_p,
        ]);
        $id = $request->user_id;
        return redirect()->route('getListPromotionOfOwner',[$id]);
    }

    public function getEditPromotionOfOwner($id){
        $promotion = Promotion::find($id);
        return view('owner.edit_promotion',['promotion'=>$promotion,'id'=>$id]);
    }

    public function postPromotionEditOfOwner(Request $request,$id){
        $post = Promotion::find($id);
        $post->name = $request->name;
        $post->value_p = $request->value_p;
        $post->save();
        $user_id = $post->user_id;
        return redirect()->route('getListPromotionOfOwner',[$user_id]);
    }
    public function deleteOfOwnerPromotion($id){
        $promotion = Promotion::find($id);
        $user_id = $promotion['user_id'];
        $promotion->delete($id);
        return redirect()->back();
    }
}


