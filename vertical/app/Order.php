<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='order';

    public function order_detail(){
        return $this->hasOne('App\Order_detail');
    }
}
