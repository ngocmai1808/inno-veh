@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-8 col-xs-offset-2">
	<div class="panel panel-primary">
		<div class="panel-heading">Edit User</div>
		<div class="panel-body">

			<form method="POST" action="{{route('postUserEdit',['id'=>$id])}}" enctype= "multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">name</div>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" value="{{ isset($user->name) ?$user->name:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">email</div>
				<div class="col-md-9">
					<input type="text" name="email" class="form-control" value="{{ isset($user->email) ?$user->email:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">role</div>
				<div class="col-md-9">
					<input type="text" name="role_id" class="form-control" value="{{ isset($user->role_id) ?$user->role_id:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">password</div>
				<div class="col-md-9">
					<input type="text" name="password" class="form-control" value="{{ isset($user->password) ?$user->password:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">phone</div>
				<div class="col-md-9">
					<input type="text" name="phone" class="form-control" value="{{ isset($user->phone) ?$user->phone:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">adress</div>
				<div class="col-md-9">
					<input type="text" name="address" class="form-control" value="{{ isset($user->address) ?$user->address:'' }}">
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">detail</div>
				<div class="col-md-9">
					<textarea name="detail" class="form-control" style="height:250px;">	
					{{ $user->detail }} 
					</textarea>
					<script type="text/javascript">
					CKEDITOR.replace('detail');
					</script>
				</div>
			</div>
			<!-- end row -->
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">status</div>
				<div class="col-md-9">
					<input type="number" name="status" value="{{ $user->status}}">
				</div>
			</div>
			
			<!-- end row -->			
			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3">Ảnh</div>
				<div class="col-md-9">
					<input type="file" name="image">
				</div>
			</div>
			
			<!-- end row -->			
			<!-- row -->
			<div class="row" style="margin-top:5px;">
				<div class="col-md-3"></div>
				<div class="col-md-9">
					<input type="submit" class="btn btn-primary" value="Process">
				</div>
			</div>
			<!-- end row -->
			</form>
		</div>
	</div>
</div>
@endsection