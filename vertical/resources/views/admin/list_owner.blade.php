@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-12 col-xs-offset-0">
	<div class="panel panel-primary">
		<div class="panel-heading">List Owner</div>
		<div style="margin:15px 10px">
			<a href="{{ url('admin/owner/add')}}" class="btn btn-primary">Add</a>
		</div>
		<div class="panel-body">
			<table class="table table-bordered table-hover">
				
				<tr>
					<th style="width:20px;">STT</th>
					<th style="width: 50px;">name</th>
					<th style="width: 20px;">email</th>
					<th style="width: 30px;">Role</th>
					<th style="width: 100px;">phone</th>
					<th style="width: 100px;">image</th>
					<th style="width :30px;">status</th>
					<th style="width: 100px;">address</th>
					<th style="width: 100px;">detail</th>
					<th style="width:200px;">Thao tác</th>
				</tr>
				<?php $stt = 0; ?>
				@foreach( $owners as $owner)
				<?php $stt++; ?>
				<tr>
					<td>{{ $stt }}</td>
					<td>{{ $owner->name }} </td>
					<td>{{ $owner->email }}</td>
					<td>{{ $owner->role_id }}</td>
					<td>{{$owner->phone}}</td>
					<td>
						@if(file_exists("upload/owner/".$owner->image))
						<img style="width: 100px; height: 100px;" src="{{ asset('upload/owner/'.$owner->image)}}">
						@endif
					</td>
					<td>{{$owner->status}}</td>

					<td>{{ $owner->address}}</td>
					<td>{!!$owner->detail!!}</td>
					<td style="text-align:center">
						<a href="{{url('admin/owner/edit/'.$owner->id)}}">Edit</a>&nbsp;|&nbsp;
						<a href="{{url('admin/owner/delete/'.$owner->id)}}" onclick="return window.confirm('Are you sure?');">Delete&nbsp;|&nbsp;</a>
						<a  href="{{url('admin/motorbike/list/'.$owner->id)}}">List Motobike</a>
						<a  href="{{url('admin/promotion/list/'.$owner->id)}}">List Promotion</a>
						<a  href="{{url('admin/promotion/list/'.$owner->id)}}">revenue</a>
					</td>
				</tr>
				@endforeach
			</table>
			<style type="text/css">
				.pagination{padding:0px; margin:0px;}
			</style>
			
	</div>
</div>
@endsection