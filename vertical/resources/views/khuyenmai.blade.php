<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Khuyến mãi</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}">
    <!--Front-Awesome-->
    <link rel="stylesheet" href="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?fbclid=IwAR2RxPg5srJ2_UhPB6BSgr-lvfxz4_AylOeRXhXkqZ9btpjAt7DppQ3Ad8I')}}">
    <!--style-->
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
</head>
<body>
<!--header-->
<header>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md">
            <!-- Brand -->
            <a class="navbar-brand" href="{{URL::route('home')}}"><img class="h-logo" src="images/Logo.png" alt="Logo"></a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler ml-auto" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="h-link nav-link" href="#">Giới thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class="h-email nav-link" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><img src="images/bn-vietnam.jpg" alt="VN"></a>
                    </li>
                    <li class="nav-item">
                        <div class="h-user dropdown">
                            <button class="h-user__btn dropdown-toggle" type="button" data-toggle="dropdown">
                                <img class="h-user__avt" src="./images/avatar.JPG" alt="avatar">
                                <span class="h-user__name">Trần Văn Nam</span>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{URL::route('customer')}}">Trang cá nhân</a>
                                <a class="dropdown-item" href="{{URL::route('dangkychuxe')}}">Làm nhà xe</a>
                                <a class="dropdown-item" href="#">Đăng xuất</a>
                            </div>
                        </div>
                    </li>
                    <!-- sign -->
                </ul>
            </div>
        </nav>
    </div>
</header>

<!--owner-nav-->
<div class="container">
    <nav class="owner-nav">
        <!-- Links -->
        <ul class="owner-nav__list">
            <li class="owner-nav__item">
                <a class="owner-nav__link active" href="{{URL::route('danhsachthuexe')}}">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <span class="owner-nav__txt">Danh sách thuê xe</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('lich')}}">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    <span class="owner-nav__txt">Lịch</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('danhgia')}}">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    <span class="owner-nav__txt">Đánh giá </span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('bieudo')}}">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    <span class="owner-nav__txt">Biểu đồ</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('khuyenmai')}}">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    <span class="owner-nav__txt">Khuyến mãi</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('nhaxe')}}">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    <span class="owner-nav__txt">Thông tin nhà xe</span>
                </a>
            </li>
        </ul>
    </nav>
</div>

<!--khuyenmai.html-->

<!--TODO-->
<!--only HTML-->
<!--css in comment khuyenmai.html-->

<section class="discounts">
    <div class="container">
     
        <div class="row">
            <div class="col-md-4">
                @foreach($khuyenmai as $km)
                <div class="discounts__item">
                    @foreach($imgkhuyenmai as $imgkm)
                    <img class="img-fluid" src="./images/img-xe.jpg" alt="ảnh xe">
                    <p class="discounts__txt">{{$km->name}}</p>
                    <p class="discounts__txt">Nhà xe: {{$imgkm->name}} giảm giá {{$km->value}}</p>
                    @endforeach
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <select class="form-control">
                            <option>Đang áp dụng</option>
                            <option>Chưa áp dụng</option>
                            <option>Hết hạn</option>
                        </select>
                        <span class="icon-edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</section>

<!--footer-->
<footer class="f-sub">
    <ul class="f-sub-nav">
        <li><a href="#">Về chúng tôi</a><span>|</span></li>
        <li><a href="#">Điều khoản</a><span>|</span></li>
        <li><a href="#">Chính sách bảo mật</a></li>
    </ul>
    <p class="f-sub-copyright">Copyright &copy; 2019 <span>INNO VEH</span> </p>
</footer>


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--javascript-->
<script src="{{URL::asset('js/main.js')}}"></script>
</body>
</html>
