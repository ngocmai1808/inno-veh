<div class="container-fluid">
    <nav class="navbar navbar-expand-md">
        <!-- Brand -->
        <a class="navbar-brand wow fadeInUp" data-wow-delay="0s" data-wow-duration="1.5s" href="{{URL::route('home')}}"><img class="h-logo" src="{{asset('images/Logo.png')}}" alt="Logo"></a>
        <div class="header-menu">
            <!-- Toggler/collapsibe Button -->
            <nav class="navbar navbar-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
            <div class="collapse" id="navbarToggleExternalContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="h-link nav-link" href="#">Giới thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class=" nav-link" href="#">Thông báo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><img src="{{asset('images/bn-vietnam.jpg')}}" alt="VN"></a>
                    </li>
                    <li class="nav-item dangnhap">
                        {{--@if(Auth::check())--}}
                        {{--<li class="nav-item">--}}
                            {{--<div class="h-user dropdown">--}}
                                {{--<button class="h-user__btn dropdown-toggle" type="button" data-toggle="dropdown">--}}
                                    {{--<img class="h-user__avt" src="./images/{{Auth::user()->image}}" alt="avatar">--}}
                                    {{--<span class="h-user__name">{{Auth::user()->name}}</span>--}}
                                {{--</button>--}}
                                {{--<div class="dropdown-menu">--}}
                                    {{--<a class="dropdown-item" href="{{URL::route('customer')}}">Trang cá nhân</a>--}}
                                    {{--<a class="dropdown-item" href="{{URl::route('danhsachthuexe')}}">Làm nhà xe</a>--}}
                                    {{--<a class="dropdown-item" href="#">Đăng xuất</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--@else--}}
                        <a href="{{URL::route('login')}}" class="h-link nav-link">Đăng kí <span>/</span>Đăng nhập</a>
                        {{--@endif--}}
                    </li>
                    <!-- sign -->
                </ul>
            </div>
        </div>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse wow fadeInUp" data-wow-delay="0s" data-wow-duration="1.5s" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="h-link nav-link" href="#">Giới thiệu</a>
                </li>
                <li class="nav-item">
                    <a class="h-email nav-link" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><img src="{{asset('images/bn-vietnam.jpg')}}" alt="VN"></a>
                </li>
                <li class="nav-item dangnhap">
                    <a href="" class="h-link nav-link">Đăng kí <span>/</span>Đăng nhập</a>

                </li>
                <!-- sign -->
            </ul>
        </div>
    </nav>
</div>
</header>