<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Danh sách thuê xe</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}">
    <!-- pgwSlider -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/pgwslider.min.css')}}">
    <!--Font-Awesome 4-->
    <link rel="stylesheet" href="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?fbclid=IwAR2RxPg5srJ2_UhPB6BSgr-lvfxz4_AylOeRXhXkqZ9btpjAt7DppQ3Ad8I')}}">
    <!--owl-carousel-->
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
    <!--style-->
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
</head>
<body>

<!--header-->
@include('layouts.header')

<div class="container">
    <nav class="owner-nav">
        <!-- Links -->
        <ul class="owner-nav__list">
            <li class="owner-nav__item">
                <a class="owner-nav__link active" href="{{URL::route('danhsachthuexe')}}">
                    <img src="images/ds.png" alt="" class="img-responsive">
                    <span class="owner-nav__txt">Danh sách thuê</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('lich')}}">
                    <img src="images/lichb.png" alt="" class="img-responsive">
                    <span class="owner-nav__txt">Lịch</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('danhgia')}}">
                    <img src="images/danhgia.png" alt="" class="img-responsive">
                    <span class="owner-nav__txt">Đánh giá </span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URl::asset('bieudo')}}">
                    <img src="images/bd.png" alt="" class="img-responsive">
                    <span class="owner-nav__txt">Biểu đồ</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('khuyenmai')}}">
                    <img src="images/km.png" alt="" class="img-responsive">
                    <span class="owner-nav__txt">Khuyến mãi</span>
                </a>
                <!--<span class="owner-nav__line">|</span>-->
            </li>
            <li class="owner-nav__item">
                <a class="owner-nav__link" href="{{URL::route('nhaxe')}}">
                    <img src="images/thongtinb.png" alt="" class="img-responsive">
                    <span class="owner-nav__txt">Thông tin nhà xe</span>
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--danh sach thue xe-->
<section class="list-booked">
    <div class="owner-tab-wrap container-fluid">
      <div class="container">
          <ul class="owner-tab nav nav-tabs">
              <li class="owner-tab__item owner-tab__item--active active"><a data-toggle="tab" href="#chuanhanxe">Chưa nhận xe</a></li>
              <li class="owner-tab__item"><a data-toggle="tab" href="#nhanxe">Đã nhận xe</a></li>
              <li class="owner-tab__item"><a data-toggle="tab" href="#traxe">Đã trả xe</a></li>
          </ul>
      </div>
    </div>
    <div class="container">
        <div class="tab-content">
            <div id="chuanhanxe" class="tab-pane active">
                <div class="row">
                    <h6 class="col-8">Danh sách sắp xếp theo ngày đặt</h6>
                    <label class="col-4">
                        <select name="cars" class="custom-select mb-3">
                            <option selected>Ngày đặt xe</option>
                            <option value="">Ngày nhận xe</option>
                        </select>
                    </label>
                </div>
                <!-- Tables begin -->
                <!--scss: generic-->
                <table class="table">
                    <thead class="table__head">
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="col">Tên khách hàng</th>
                        <th class="table__cell table__cell--head" scope="col">Ngày nhận - Ngày trả</th>
                        <th class="table__cell table__cell--head" scope="col">Mã đặt</th>
                        <th class="table__cell table__cell--head" scope="col">Loại xe</th>
                        <th class="table__cell table__cell--head" scope="col">Số lượng</th>
                        <th class="table__cell table__cell--head" scope="col">Tổng tiền</th>
                        <th class="table__cell table__cell--head" scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="table__body">

                    <tr class="table__row">
                        <th class="table__cell table__cell--head table__cell--spanned" colspan="7">
                            Ngày đặt: <span>14/03/2019</span>
                        </th>
                    </tr>

                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>

                    <tr class="table__row">
                        <th class="table__cell table__cell--head table__cell--spanned" colspan="7">
                            Ngày đặt: <span>13/03/2019</span>
                        </th>
                    </tr>

                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                           <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <th class="table__cell table__cell--head" scope="row" data-title="Tên khách hàng">
                            Trần Thị An
                        </th>
                        <td class="table__cell" data-title="Ngày nhận - Ngày trả">
                            <span>T6 15/03/2019</span> - <span>T3 19/03/2019</span>
                        </td>
                        <td class="table__cell" data-title="Mã đặt">
                            #12345
                        </td>
                        <td class="table__cell" data-title="Loại xe">
                            Xe 4 chỗ
                        </td>
                        <td class="table__cell" data-title="Số lượng">
                            2
                        </td>
                        <td class="table__cell" data-title="Tổng tiền">
                            2.000.000 VND
                        </td>
                        <td class="table__cell">
                            <a class="btn-detail dinhvi" href="">Xem định vị</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--end-table-->
            </div>
            <div id="nhanxe" class="tab-pane fade">
            </div>
            <div id="traxe" class="tab-pane fade">
            </div>
        </div>
    </div>
</section>

<!--footer-->
@include('layouts.footer')

<div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d119213.26008557116!2d105.76475713124321!3d20.976020360536133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acce762c2bb9%3A0xbb64e14683ccd786!2zSOG7jWMgdmnhu4duIEPDtG5nIG5naOG7hyBCxrB1IGNow61uaCBWaeG7hW4gdGjDtG5nIC0gUFRJVA!5e0!3m2!1svi!2s!4v1560237762922!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    <i class="fa fa-window-close" aria-hidden="true"></i>

</div>
<!-- jQuery library -->
<script src="{{URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js')}}"></script>
<!-- Latest compiled JavaScript -->
<script src="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
<!-- pgwSlider -->
<script type="text/javascript" src="{{URL::asset('js/pgwslider.min.js')}}"></script>
<!--javascript-->
<script src="{{URL::asset('js/main.js')}}"></script>
</body>
</html>
