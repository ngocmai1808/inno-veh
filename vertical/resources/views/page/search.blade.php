<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <title>Danh sách nhà xe</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}">
    <!--Front-Awesome-->
    <link rel="stylesheet" href="{{URl::asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?fbclid=IwAR2RxPg5srJ2_UhPB6BSgr-lvfxz4_AylOeRXhXkqZ9btpjAt7DppQ3Ad8I')}}">
    <!--owl-carousel-->
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
    <!--style-->
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
</head>

<body>
<!--header-->
@include('layouts.header')

<!--g-nav-->
<nav class="g-nav">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link active" href="#">Thuê xe</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled c-main" href="#">Thuê cano</a>
        </li>
    </ul>
</nav>

<!--info-search-->
<section class="info">
    <div class="container">
        <div class="row">
            <div class="info__left col-6">
                <p class="info__txt">Hà Nội</p>
                <p class="info__txt">Xe ô tô - Tự lái</p>
                <p class="info__txt">4 - 7 chỗ ngồi</p>
            </div>

            <div class="info__right col-6">
                <label class="info__date">
                    <input class="form-control" type="date">
                </label>
                <label class="info__date">
                    <input class="form-control" type="date">
                </label>
            </div>
        </div>
    </div>
</section>


<!--show-search-->
<section class="show-search">
    <div class="container">
        <div class="show-search__top row">
            <div class="col-6">
                <button class="btn-mapview"><span class="icon-map"></span>Map view</button>
                <span>Tìm thấy {{count($srchnxe)}} kết quả</span>
            </div>
            <div class="col-6">
                <form>
                    <label>Bộ lọc
                        <select name="filter" class="custom-select">
                            <option selected>Đánh giá</option>
                            <option value="">Giá cả</option>
                            <option value="">Hot deal</option>
                        </select>
                    </label>
                </form>
            </div>
        </div>

        <div class="owner">
            @foreach($srchnxe as $mt)
                <div class="owner__item">
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="owner__img">
                                <img src="./images/product/{{$mt->image}}" alt="xe">
                            </div>
                        </div>
                        <div class="owner__info col-md-8 col-lg-5">
                            <p class="font-weight-bold">{{$mt->name}}</p>
                            <p><span class="font-weight-bold">Địa chỉ:</span> {{$mt->address}}</p>
                            <p><span class="font-weight-bold">Giá:</span>Khoảng từ 1.000.000đ - 2.000.000đ</p>
                            @if($mt->status == 1)
                                <p><span class="font-weight-bold">Tình Trạng: </span>Còn xe</p>
                            @else
                                <p><span class="font-weight-bold">Tình Trạng: </span>Hết xe</p>
                            @endif
                        </div>
                        <div class="owner__info owner__info--left col-md-4 col-lg-3">
                            <div class="owner__rating g-rating">
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <p>10 người đánh giá</p>
                            </div>
                            <div class="owner__btn">
                                <a href="{{URL::route('chitietsanpham',[$mt->id])}}">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
</section>
@include('layouts.lgvsrgt')
<!--footer-->
@include('layouts.footer')




<!-- jQuery library -->
<script src="{{URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js')}}"></script>
<!-- Latest compiled JavaScript -->
<script src="{{URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
<!--javascript-->
<script src="{{URL::asset('js/main.js')}}"></script>
</body>
</html>