<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Hash;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLayout(){

        return view('admin.layout');
    }
    public function getLayoutOwner(){

        return view('admin.layoutOwner');
    }
    public function getAll(){
            $owners = User::where('role_id',2)->get();
        return view('admin.list_owner',compact('owners'));
    }

    public function getEdit($id){
        $owner = User::find($id);
        return view('admin.edit_owner',['owner'=>$owner]);
    }

    public function postEdit(UserRequest $request,$id){
    
            $post = User::find($id);
            $image='upload/'.$request->image;
            $post->name = $request->name;
            $post->role_id= $request->role_id;
            $post->phone=$request->phone;
            $post->password= Hash::make($request->password); 
            $post->email= $request->email;
            $post->status=$request->status;
            $post->address= $request->address;
            $post->detail= $request->detail;
            $post->save();
            return redirect('admin/owner');
    }

    public function getAdd(){
        return view('admin.add_owner');
    }
    public function postAdd(UserRequest $request){
       if($request->hasFile('image')){
        $image = $request->file('image');
        //upload anh
         $image->move("upload/owner",$image->getClientOriginalName());
         $add = User::create([
        'name' => $request->name,
        'role_id' => $request->role_id,
        'phone' => $request->phone,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'status' => $request->status,
        'detail'=> $request->detail,
        'image'=>$image->getClientOriginalName(),
        'address'=> $request->address,
      ]); 
       return redirect('admin/owner')->with('mesage','Chủ xe đã được thêm thành công');
      }    
    }
    public function delete($id){
        $owner = User::find($id); 
        $owner->delete($id);
    return redirect('admin/owner');

    }

    // người dùng 
    public function getUserAll(){
        $users = User::where('role_id',3)->get();
        return view('admin.list_user',compact('users'));
    }
    public function getUserAdd(){
        return view('admin.add_user');
    }
    public function postUserAdd(UserRequest $request){
       if($request->hasFile('image')){
        $image = $request->file('image');
        //upload anh
         $image->move("upload/user",$image->getClientOriginalName());
         $add = User::create([
        'name' => $request->name,
        'role_id' => $request->role_id,
        'phone' => $request->phone,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'status' => $request->status,
        'detail'=> $request->detail,
        'image'=>$image->getClientOriginalName(),
        'address'=> $request->address,
      ]); 
       return redirect('/admin/user')->with('mesage','Chủ xe đã được thêm thành công');
      }    
    }
     public function getUserEdit($id){
        $user = User::find($id);
        return view('admin.edit_user',['user'=>$user,'id'=>$id]);
    }

    public function postUserEdit(Request $request,$id){
            $post = User::find($id);
            $image='upload/'.$request->image;
            $post->name = $request->name;
            $post->role_id= $request->role_id;
            $post->phone=$request->phone;
            $post->password= Hash::make($request->password);
            $post->email= $request->email;
            $post->status=$request->status;
            $post->address= $request->address;
            $post->detail= $request->detail;
            $post->save();
           return redirect('admin/user');
    }
    public function userdelete($id){
        $owner = User::find($id); 
        $owner->delete($id);
    return redirect('admin/user');

    }
}
