<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permission_role', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
        });
        Schema::table('users',function(Blueprint $table){
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
        
        Schema::table('motorbikes',function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('promotions',function(Blueprint$table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('images',function(Blueprint $table){
            $table->foreign('motorbike_id')->references('id')->on('motorbikes')->onDelete('cascade');
        });

        Schema::table('reviews',function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('order_detail',function(Blueprint $table){
            $table->foreign('order_id')->references('id')->on('order')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('permission_role', function (Blueprint $table) {
           $table->dropForeign(['role_id','permission_id']);
           });
        Schema::table('users',function(Blueprint $table){
            $table->dropForeign(['role_id']);
        });
        Schema::table('motorbikes',function(Blueprint $table){
            $table->dropForeign(['user_id']);
        });
        Schema::table('promotions',function(Blueprint$table){
            $table->dropForeign(['user_id']);
        });
        Schema::table('images',function(Blueprint $table){
            $table->dropForeign(['motorbike_id']);
        });
        Schema::table('reviews',function(Blueprint $table){
            $table->dropForeign(['user_id']);
        });
         Schema::table('order_detail',function(Blueprint $table){
            $table->dropForeign(['order_id']);
        });
    }
}
