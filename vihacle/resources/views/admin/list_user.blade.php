@extends("admin.layout")
@section("do-du-lieu")
<div class="col-md-12 col-xs-offset-0">
	<div class="panel panel-primary">
		<div class="panel-heading">List user</div>
		<div style="margin:15px 10px">
			<a href="{{ url('admin/user/add')}}" class="btn btn-primary">Add</a>
		</div>
		<div class="panel-body">
			<table class="table table-bordered table-hover">
				
				<tr>
					<th style="width:20px;">STT</th>
					<th style="width: 50px;">name</th>
					<th style="width: 20px;">email</th>
					<th style="width: 30px;">Role</th>
					<th style="width: 100px;">phone</th>
					<th style="width: 100px;">image</th>
					<th style="width :30px;">status</th>
					<th style="width: 100px;">address</th>
					<th style="width: 100px;">detail</th>
					<th style="width:200px;">Thao tác</th>
				</tr>
				<?php $stt = 0; ?>
				@foreach( $users as $user)
				<?php $stt++; ?>
				<tr>
					<td>{{ $stt }}</td>
					<td>{{ $user->name }} </td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->role_id }}</td>
					<td>{{$user->phone}}</td>
					<td>
						@if(file_exists("upload/user/".$user->image))
						<img style="width: 100px; height: 100px;" src="{{ asset('upload/user/'.$user->image)}}">
						@endif
					</td>
					<td>{{$user->status}}</td>

					<td>{{ $user->address}}</td>
					<td>{!!$user->detail!!}</td>
					<td style="text-align:center">
						<a href="{{url('admin/user/edit/'.$user->id)}}">Edit</a>&nbsp;|&nbsp;
						<a href="{{url('admin/user/delete/'.$user->id)}}" onclick="return window.confirm('Are you sure?');">Delete</a>
					</td>
				</tr>
				@endforeach
			</table>
			<style type="text/css">
				.pagination{padding:0px; margin:0px;}			
			</style>
			
	</div>
</div>
@endsection